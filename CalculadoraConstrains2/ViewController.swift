//
//  ViewController.swift
//  CalculadoraConstrains2
//
//  Created by Pedro Ferreira on 16/10/20.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ButtonConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var TotalResultado: UILabel!
    @IBOutlet weak var TotalDescuento: UILabel!
    
    @IBOutlet weak var Porcentaje: UITextField!
    @IBOutlet weak var Cantidad: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pantalla()
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(teclado(notificacion:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(teclado(notificacion:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(teclado(notificacion:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    func pantalla(){
        if UIDevice().userInterfaceIdiom == .phone{
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("Iphone 5 o SE")
            case 1334:
                print("Iphone 6, 6s, 7.8,")
            case 2208:
                print("Iphone PLUS")
            case 2436:
                print("Iphone X")
            case 1792:
                print("Iphone 11")
            case 2688:
                print ("Iphone XS MAX")
            default:
                print ("Cualquier otro tama;o")
            }
            }
        }
    @objc func teclado(notificacion:Notification){
        guard let tecladoUp = (notificacion.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue  else { return }
        if notificacion.name == UIResponder.keyboardWillShowNotification{
            if UIScreen.main.nativeBounds.height == 1334 {
                self.view.frame.origin.y = -tecladoUp.height
            }
        }else{
            self.view.frame.origin.y = 0
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func Calcular(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let guardarcantidad = Cantidad.text else { return}
        guard let guardarporcentaje = Porcentaje.text else { return }
        let cant = (guardarcantidad as NSString).floatValue
        let porcenta = (guardarporcentaje as NSString).floatValue
        let desc = cant * porcenta / 100
        let totalfinal = cant - desc
        TotalResultado.text = "$\(totalfinal)"
        TotalDescuento.text = "$\(desc)"
        
    }
    
    @IBAction func Limpiar(_ sender: UIButton) {
        self.view.endEditing(true)
        TotalResultado.text = "$0.00"
        TotalDescuento.text = "$0.00"
        Porcentaje.text = ""
        Cantidad.text = ""
    }
    
}

